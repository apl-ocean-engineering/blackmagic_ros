#include <pluginlib/class_list_macros.h>

#include "blackmagic_ros/BlackmagicNodelet.h"

#include "libblackmagic/OutputHandler.h"

#include "libbmsdi/helpers.h"

#include "g3_to_ros_logger/g3logger.h"

namespace blackmagic_ros {

using namespace libblackmagic;


BlackmagicNodelet::BlackmagicNodelet()
  : Nodelet(),
    _prevConfig(),
    _client( 0 )
{;}

BlackmagicNodelet::~BlackmagicNodelet()
{
  _client.stopStreams();

}

void BlackmagicNodelet::onInit() {

  ros::NodeHandle& nh = getNodeHandle();
  ros::NodeHandle& pnh = getPrivateNodeHandle();

  blackmagic_pub_left = nh.advertise<sensor_msgs::Image>("left/image_color", 100);
  blackmagic_pub_right = nh.advertise<sensor_msgs::Image>("right/image_color", 100);

  _client.input().setNewImagesCallback( std::bind( &BlackmagicNodelet::newImageCallback, this, std::placeholders::_1 ) );

  _dynCfgServer.setCallback( boost::bind(&BlackmagicNodelet::configCallback, this, _1, _2) );

   if( !_client.startStreams() ) {
     ROS_ERROR("Unable to start streams");
   }

}




void BlackmagicNodelet::configCallback(blackmagic_ros::BlackmagicCamerasConfig &config, uint32_t level){
  	// static uint8_t currentGain = 0x1;
  	// static uint32_t currentExposure = 0;  // Start at 1/60

  	//shared_ptr<SharedBMSDIBuffer> sdiBuffer(  );
  	SDIBufferGuard guard( _client.output().sdiProtocolBuffer() );

    const int camnum = 0;

    // Manual focus
    if (config.focus != _prevConfig.focus){
      const double val = config.focus;
      LOG(INFO) << "Setting focus to " << val;
      guard( [&camnum, &val]( BMSDIBuffer *buffer ){	bmAddFocus( buffer, camnum, val); });
    }

    // Set autofocus
    if(config.autofocus){
      LOG(INFO) << "Restoring  adding autofocus";
      guard( [&camnum]( BMSDIBuffer *buffer ){ bmAddInstantaneousAutofocus( buffer, camnum ); });
    }

    //== Autoexposure mode
    if (config.autoexposure_mode != _prevConfig.autoexposure_mode){
      const auto val = config.autoexposure_mode;
      LOG(INFO) << "Setting autoexposure mode to " << val;
      guard( [&camnum, &val]( BMSDIBuffer *buffer ){	bmAddAutoExposureMode( buffer, camnum, val ); });
    }


    //== Aperture ==
    if (config.aperture != _prevConfig.aperture){
      int val = config.aperture; //Get average step size to 1.0
      LOG(INFO) << "Adjusting aperture by " << val;
      guard( [&camnum, &val]( BMSDIBuffer *buffer ){	bmAddApertureFStop( buffer, camnum, val); });
    }

    //== Shutter ==

    if (config.exposure != _prevConfig.exposure){
      const int val = config.exposure;
      LOG(INFO) << "Setting exposure ordinal " << val;
      guard( [&camnum, &val]( BMSDIBuffer *buffer ){	bmAddExposureOrdinal( buffer, camnum, val ); });
    }

    // I didn't find shutter speed to be effective,
    // if (config.shutter_speed != _prevConfig.shutter_speed){
    //   const int val = config.shutter_speed;
    //   LOG(INFO) << "Setting shutter speed to 1/" << val;
    //   guard( [&camnum, &val]( BMSDIBuffer *buffer ){	bmAddShutterSpeed( buffer, camnum, val ); });
    // }

    //== Gain ==
    if (config.gain != _prevConfig.gain){
      const auto val = config.gain;
      LOG(INFO) << "Setting gain to " << val;
      guard( [&camnum, &val]( BMSDIBuffer *buffer ){	bmAddSensorGain( buffer, camnum, val ); });
    }

    //== White balance ==

    //White balance
    if (config.white_balance != _prevConfig.white_balance){
      const int val = config.white_balance; //Get average step size to 500.0
      LOG(INFO) << "Setting white_balance temperature to " << val;
      guard( [&camnum, &val]( BMSDIBuffer *buffer ){	bmAddWhiteBalance( buffer, camnum, val, 0 ); });
    }

    // Set auto restore white focus
    if (config.restore_white_balance){
      LOG(INFO) << "Restoring white balance";
      guard( [&camnum]( BMSDIBuffer *buffer ){	bmAddRestoreWhiteBalance( buffer, camnum ); });
    }

    // This doesn't seem to do anything
    // //== Overlay mode ==
    // if (config.status_overlay != _prevConfig.status_overlay){
    //   const int val = config.status_overlay; //Get average step size to 500.0
    //   LOG(INFO) << "Setting display overlay to " << (val ? "on" : "off");
    //   guard( [&camnum, &val]( BMSDIBuffer *buffer ){	bmAddOverlayEnable( buffer, camnum, (val ? 0x0003 : 0x0000) ); });
    // }

    _prevConfig = config;

}

void BlackmagicNodelet::newImageCallback( const InputHandler::MatVector &rawImages )
{
  const auto currentTime = ros::Time::now();

  //\TODO Ugly-ish
  if( rawImages.size() >= 1 ) {
    cv_bridge::CvImage img_bridge;
    sensor_msgs::Image output_msg;

    std_msgs::Header header;
    header.seq = _count;
    header.stamp = currentTime;
    img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::BGRA8, rawImages[0] );
    img_bridge.toImageMsg(output_msg);
    blackmagic_pub_left.publish(output_msg);
  }

  if( rawImages.size() >= 2 ) {
    cv_bridge::CvImage img_bridge;
    sensor_msgs::Image output_msg;

    std_msgs::Header header;
    header.seq = _count;
    header.stamp = currentTime;
    img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::BGRA8, rawImages[1] );
    img_bridge.toImageMsg(output_msg);
    blackmagic_pub_right.publish(output_msg);
  }

  ++_count;
}


} // End namespace

PLUGINLIB_EXPORT_CLASS(blackmagic_ros::BlackmagicNodelet, nodelet::Nodelet)
