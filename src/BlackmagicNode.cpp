#include <ros/ros.h>
#include <nodelet/loader.h>

#include "g3_to_ros_logger/g3logger.h"
#include "g3_to_ros_logger/ROSLogSink.h"


int main(int argc, char **argv) {
  libg3logger::G3Logger<ROSLogSink> logWorker(argv[0]);
  logWorker.logBanner();
  logWorker.setLevel( DEBUG );

  ros::init(argc, argv, "blackmagic");

  nodelet::Loader manager(true);
  nodelet::M_string remappings;
  nodelet::V_string my_argv;

  manager.load(ros::this_node::getName(), "blackmagic_ros/BlackmagicNodelet", remappings, my_argv);

  ros::spin();
}
