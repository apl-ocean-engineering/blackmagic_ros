#pragma once

#include <ros/ros.h>
#include <nodelet/nodelet.h>

// For converting opencv Mat to ROS Image format
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <string>
#include <thread>
using namespace std;

#include <dynamic_reconfigure/server.h>
#include "blackmagic_ros/BlackmagicCamerasConfig.h"

#include "libblackmagic/InputOutputClient.h"

namespace blackmagic_ros {

class BlackmagicNodelet : public nodelet::Nodelet {
public:

  BlackmagicNodelet();
  virtual ~BlackmagicNodelet();

  virtual void onInit();

  void configCallback(blackmagic_ros::BlackmagicCamerasConfig &config, uint32_t level);

private:
  // Callbacks for blackmagic client
  void newImageCallback( const libblackmagic::InputHandler::MatVector &rawImages );

  blackmagic_ros::BlackmagicCamerasConfig _prevConfig;
  dynamic_reconfigure::Server<blackmagic_ros::BlackmagicCamerasConfig> _dynCfgServer;

  libblackmagic::InputOutputClient _client;

  int _camNum, _count;

  ros::Publisher blackmagic_pub_left;
  ros::Publisher blackmagic_pub_right;

};

}
