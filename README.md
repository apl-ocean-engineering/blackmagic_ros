# blackmagic_ros
ROS node that uses [libblackmagic](https://github.com/apl-ocean-engineering/libblackmagic) to publish images from blackmagic cameras to ROS topics.
Currently runs on ROS Melodic, Ubuntu 18.04

## Installation
  Note: libblackmagic requires the Blackmagic_DeckLink_SDK; all references to it in this repo use the specific location of where it is found on the machine used to make the repo.
  1. Clone [libblackmagic](https://github.com/apl-ocean-engineering/libblackmagic) to <catkin_ws>/src
  2. As per the install instructions on the libblackmagic page, run the following commands from <catkin_ws>/src/libblackmagic:  
    *./fips fetch*  
    *./fips gen*  
    *./fips build*  
    This may require an environment variable BLACKMAGIC_DIR pointing to where the Blackmagic_DeckLink_SDK is located
  3. Clone this repo to <catkin_ws>/src and run the command *catkin_make*


## Usage
Running the command *roslaunch blackmagic_ros bm_node.launch* will start the publisher node as well as rqt_image_view


# License
